## SkillBox DevOps. Инфраструктурная платформа на основе Kubernetes. Часть 3. Безопасность и управление доступом

SkillBox DevOps. Инфраструктурная платформа на основе Kubernetes. Часть 3. Безопасность и управление доступом

--------------------------------------------------------

Сценарий работы с репозиторием:
```
cd existing_repo
git remote add origin git@gitlab.com:sb721407/kubernetes/k4.git
git branch -M main
git push -uf origin main
```

### Задание 1

https://gitlab.com/sb721407/kubernetes/k4/-/tree/main/role-binding.yaml


### Задание 2 

https://gitlab.com/sb721407/kubernetes/k4/-/tree/main/psp.yaml
